<?php
require 'class/CassetteEnquire.php';

$email = $_POST['em']; // REQUIRED
$phone = $_POST['pnumber'];
$comment = $_POST['comment'];
$name = $_POST['fname'];
$nameLast = $_POST['lname_1'];
$msg = 'Name: '.$name.' '.$nameLast."<br/>".
    'Email: '.$email."<br/>".
    'Contact Phone Number: '.$phone."<br/>".
    'Additional Comments: '.$comment."<br/>";

if (!$email)
    exit("error : no email received");


$cassetteEnquire = new CassetteEnquire();
$data = array();
$errors = array();






// EMAIL SETUP
$data['email'] = array();
$data['email']['senderName'] = $name; // REQUIRED
$data['email']['senderEmail'] = $email; // REQUIRED
$data['email']['subject'] = 'Message from 58 Washington Website'; // REQUIRED
$data['email']['body'] = $msg;

// EMAIL RECIPIENTS
// EXAMPLE
$data['email']['recipients'] = array();
//$data['email']['recipients'][] = array("email"=>'mc@marshallwhite.com.au', "name" => " "); // REQUIRED
$data['email']['recipients'][] = array("email"=>'joel@cassette.com.au', "name" => "Joel Edwards"); // REQUIRED
$errors[] = $cassetteEnquire->Email($data['email']);

if ($cassetteEnquire->error_flag){
    print_r(json_encode(array("error" => true, "data" => $errors)));
}
else{
print_r(json_encode(array("error" => false, "data" => "")));
}
?>