<?php
/**
 * Created by PhpStorm.
 * User: cassette
 * Date: 18/07/2014
 * Time: 11:06 AM
 */


error_reporting(E_ALL);
require 'vendors/MyLogPHP-1.2.1.class.php';
require 'vendors/mandrill/lib/Mandrill.php';
include_once(__DIR__.'/../CassetteEnquireConfig.php');

class CassetteEnquire {

    private $logger;
    private $mailer;
    private $logMe;
    public  $error_flag;


    function __construct(){

        // LOGGER INIT
        $this->error_flag = false;

        // CREATE FOLDER LOGS IF DOESN'T EXIST
        if (!is_dir($_SERVER['DOCUMENT_ROOT']."/logs/"))
            mkdir($_SERVER['DOCUMENT_ROOT']."/logs/");
        $this->logger = new MyLogPHP($_SERVER['DOCUMENT_ROOT']."/logs/".LOGFILE);
        // MAIL INIT
        $this->mailer = new Mandrill(MANDRILL_API_KEY);
        $this->logMe = LOGGING;

    }

    private function  buildReturn($type,$module, $data = ""){
        if ($type == "error")
            $this->error_flag = true;
        return array('result' => $type, "module" => $module, "data" => $data);
    }


    function log($logType, $fromModule, $data, $error = ""){
        switch ($logType) {
            case "error":
                $this->logger->error("[".$fromModule."] [".$data. "] [". $error."]");
                break;
            case "info":
                $this->logger->info("[".$fromModule."] [".$data. ']');
                break;
        }
    }


    function CampaignMonitor($data, $logMe = true){
        if (!isset($data['url']))
            return array('result' => "error", "module" => "campaignMonitor", "data" => "URL Missing");
        $url = $data['url'];
        $extraFields = $data['fields'];
        $dataToSend = array();
        foreach($extraFields as $name => $value)
        {
            if ($value)
                $dataToSend[] = $name."=" . trim($value);
        }
        $stringToSend = implode("&", $dataToSend);
        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $stringToSend);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec( $ch );
        if(curl_errno($ch))
        {
            if ($this->logMe)
                $this->log("error", "campaignMonitor", $stringToSend, "Error Signing up");
            return $this->buildReturn("error", "campaignMonitor", "Error When Signing up");
        }
        if ($this->logMe)
            $this->log("info", "campaignMonitor", $stringToSend);

        return $this->buildReturn("success", "CampaignMonitor");
    }

    function MailChimp(){

    }

    // USING MANDRILL
    function Email($data, $logMe = true){

        try{
            $message = new stdClass();
            if (MANDRILL_SUBACCOUNT){
                $message->subaccount = MANDRILL_SUBACCOUNT;
            }
            $message->html = $data['body'];
            $message->text = $data['body'];
            $message->subject = $data['subject'];
            $message->from_email = $data['senderEmail'];
            $message->from_name  =$data['senderName'];
            $message->to = $data['recipients'];
            $message->track_opens = true;
            $response = $this->mailer->messages->send($message);
            if ($logMe)
                $this->log("info", "Mail", $data['body']);
            return $this->buildReturn("success", "mail");
    }
    catch (Mandrill_Error $e){
        if ($logMe)
            $this->log("error", "Mail", $data['body'], $e->getMessage());
        return $this->buildReturn("error", "mail", $e->getMessage());

        }
    }
}